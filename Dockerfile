FROM python:3.11
# if you are using a pypi mirror that requires authentification, you can set user credential in your .netrc file
# if you are using the normal pypi index, you must generate an empty .netrc file
COPY .netrc root/.netrc

ARG PIP_TRUSTED_HOST=pypi.org
ARG PIP_INDEX=https://pypi.org/
ARG PIP_INDEX_URL=https://pypi.org/simple/

ENV PIP_TRUSTED_HOST=${PIP_TRUSTED_HOST}
ENV PIP_INDEX=${PIP_INDEX}
ENV PIP_INDEX_URL=${PIP_INDEX_URL}

# copy the pip requirements and install them
COPY requirements.txt .

RUN pip install --upgrade pip
RUN pip install -r requirements.txt --no-cache-dir
RUN pip freeze > lock.txt

# copy necessary app files
COPY kiras_api/ kiras_api/
COPY tests/ tests/
COPY requirements-dev.txt .

COPY data/ /data/

ENTRYPOINT ["/bin/bash", "-l", "-c"]
CMD ["uvicorn kiras_api.app:app --host 0.0.0.0 --port 8000 --reload --reload-include *.json"]
