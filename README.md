# KIRAS API

The KIRAS API creates involvement scores that indicate how closely a given input text is related to an organizational unit's tasks. KIRAS API expects a list of tasks grouped by organizational unit as .json file. For a given text, the KIRAS API provides involvement scores for each organizational unit, by comparing the input texts to all tasks by each unit.
The API returns a list of organizational units ranked by the involvement scores as well as pairs of tasks and sentences from the input text as explanations of why a organizational unit has a high involvement score.

## Algorithm

The current algorithm works as follows

- Create embeddings vectors for all subtasks from GVPL
- Split input text into sentences and create embeddings vectors for them
- Compare each input sentence vector with all subtask vectors and use the maximum among the sentences for each organizationl unit as the involvement score
- The task embeddings are loaded from disk or generated if they can't be found on disk.
- The generation may take a few minutes.
- If the algorithm or the tasks json file is changed in a way that requires a regeneration of the embeddings, delete the pickled (.pkl) embedding file and embeddings will be regenerated automatically.

## Adapt to your agency

### Requirements

- Docker
- Internet access (directly or via a proxy)

### Steps

- Clone the repository
- Create a list of tasks grouped by organizational unit (Geschäftsverteilungsplan) as json file in the format documented in the data/test-tasks.json file.
- Place the file in the `data/` folder and add the name to the `gvpl_files` variable (see next step).
- All settings and configurations are set in the `kiras_api/settings.py.` You can overwrite them by setting these variables as environment variables or by an `.env` file. To do so, create a copy of `.env-example` and name it `.env` and edit the content to your needs.
- If you use a proxy, you have to set all four variables starting with "proxy_", otherwise it will be skipped. Leave blank if no proxy should be used.
- Adapt the `compose.yml` to your environment, e.g. image name or docker network settings.
- If your agency uses a different source for pip than pypi.org, you need to specify this source either in the `Dockerfile` or in the `compose.yml` as args. In the `compose.yml`, it would look something like this:

```yaml
    args:
      - ARG PIP_TRUSTED_HOST=pypi.org
      - ARG PIP_INDEX=https://pypi.org/
      - ARG PIP_INDEX_URL=https://pypi.org/simple/
```

- If this pip source needs credentials, you may put them in a `.netrc` file. You can use the `.netrc-example` file as a template, else create an empty `.netrc`.
- Start the container in the background with `docker compose up --build -d kiras-api`
- At first startup the application will download large model files from the internet and save them in a docker volume. This might take a while during first startup.

https://pip.pypa.io/en/stable/topics/authentication/

## Development (locally without Docker)

### Create and activate environment

Clone the repo and set the project root as working directory.

Create a python virtual environment with python 3.11.

Install dependencies:

```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

Install the package in editable mode:

```bash
pip install -e .
```

### Run locally

Create a copy of .env-example and name it `.env` and edit the content to your real credentials (e.g. for your proxy).

Place a custom tasks.json in the folder `data/`, or use the existing `data/test-tasks.json` for testing purposes.

Then run the application type:

``` bash
uvicorn kiras_api.app:app
```

Check the API documentation in your browser at http://localhost:8000/docs

### Run tests

```bash
pytest tests/
```

Run tests with coverage report:

```bash
pytest tests/ --cov beteiliger
```

Create html report

```bash
pytest tests/ --cov beteiliger --cov-report html
```

## Funding notice
![funding notice](assets/DE_Finanziert_von_der_Europäischen_Union_RG_POS.png)

