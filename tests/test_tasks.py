import pytest
from kiras_api.tasks import (
    join_subtasks,
    read_tasks,
    split_subtasks,
    clean_string,
)
from kiras_api.models import GVPL, Unit, Task, Subtask


@pytest.fixture
def gvpl_mock():
    return GVPL(
        name="Organisationsname",
        abbreviation="Abkürzung",
        units=[
            Unit(
                name="REF 1",
                directorate_name="A I",
                directorate_description="Unterabteilung Allgemeines",
                directorate_general_name="A",
                directorate_general_description="Abteilung Allgemeines",
                description="Referat 1",
                tasks=[
                    Task(
                        description="Aufgabe 1",
                        subtasks=[
                            Subtask(description="Unteraufgabe 1"),
                            Subtask(description="Unteraufgabe 2"),
                        ],
                    ),
                    Task(description="Aufgabe 2"),
                ],
            )
        ],
        last_exported="today",
    )


def test_clean_string():
    text = "Der Peter Pan kann fliegen, wenn er träumt."

    expected = ["peter", "pan", "kann", "fliegen", "träumt"]

    assert expected == clean_string(paragraph=text, stopwords=["der", "wenn", "er"])


def test_read_tasks():
    assert isinstance(read_tasks("./data/test-tasks.json"), GVPL)


def test_join_subtasks(gvpl_mock):
    expected = {
        "REF 1": [
            ["referat", "1", "aufgabe", "1", "unteraufgabe", "1", "unteraufgabe", "2"],
            ["referat", "1", "aufgabe", "2"],
        ]
    }

    assert join_subtasks(gvpl_mock) == expected


def test_split_subtasks(gvpl_mock):
    expected_ext = [
        "Referat 1. Aufgabe 1. Unteraufgabe 1",
        "Referat 1. Aufgabe 1. Unteraufgabe 2",
        "Referat 1. Aufgabe 2",
    ]
    expected_short = [
        "Aufgabe 1. Unteraufgabe 1",
        "Aufgabe 1. Unteraufgabe 2",
        "Aufgabe 2",
    ]

    tasks_cache = split_subtasks(gvpl_mock)
    assert tasks_cache["REF 1"]["short"] == expected_short
    assert tasks_cache["REF 1"]["extended"] == expected_ext
