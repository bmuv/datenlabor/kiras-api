import pytest
from sentence_transformers import SentenceTransformer
from kiras_api.embeddings import EmbeddingAlgo
from kiras_api.tasks import read_tasks
from kiras_api.settings import settings
from kiras_api.models import InvolvementResponse
from numpy import ndarray


@pytest.fixture
def test_algo():
    gvpls = []
    for entry in settings.gvpl_files:
        gvpls.append(read_tasks(entry))

    return EmbeddingAlgo(gvpls)


def test_load_embedding_model(test_algo):
    # this should download the model
    assert isinstance(test_algo.model, SentenceTransformer)

    # this time, model should be loaded from disk
    assert isinstance(test_algo.model, SentenceTransformer)


def test_embed_text(test_algo):
    text = "Das ist ein Text zum embedden."
    vector = test_algo.model.encode(text)
    vector = test_algo.model.encode(text)

    assert isinstance(vector, ndarray)


def test_find_involvement(test_algo):
    text = "Datenlabor muss aufgebaut werden."

    involvement = test_algo.find_involvement(text, agency="Abkürzung")

    assert isinstance(involvement, InvolvementResponse)

    # assert that referat A II 2 is suggested as it has a task "Datenlabor aufbauen"
    # see test-tasks.json
    assert involvement.scores[0].unit_name == "A II 2"
    assert involvement.scores[0].explanations[0].task == "Datenlabor aufbauen"
