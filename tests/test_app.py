"""simple tests."""
import pytest

from fastapi.testclient import TestClient
from kiras_api.app import app
from kiras_api.models import Algorithms


def test_unit_embeddings():
    with TestClient(app) as client:
        text = "Ein Datenlabor soll aufgebaut werden"
        return_n = 2

        response = client.get(
            f"/involvement?text={text}&return_n={return_n}&method={Algorithms.embeddings.value}&agency=Abkürzung"
        )
        print(response.text)

        assert response.status_code == 200
        assert response.json()["scores"][0]["unit_name"] == "A II 2"


def test_units_unknown_method():
    with TestClient(app) as client:
        text = "Peter Pan kann fliegen."
        return_n = 2

        response = client.get(
            f"/involvement?text={text}&return_n={return_n}&method=bla&agency=Abkürzung"
        )

        assert response.status_code == 422


def test_units_empty_text():
    with TestClient(app) as client:
        text = " "
        return_n = 2

        response = client.get(f"/involvement?text={text}&agency=Abkürzung")

        assert response.status_code == 400


def test_agencies():
    with TestClient(app) as client:
        response = client.get(f"/agencies")

        assert response.json()["default"] == "BMUV"

        assert len(response.json()["agencies"]) == 1
        assert response.json()["agencies"][0]["abbreviation"] == "Abkürzung"
        assert response.json()["agencies"][0]["name"] == "Organisationsname"
        assert response.json()["agencies"][0]["last_exported"] == "2023-04-21"


def test_gvpl():
    with TestClient(app) as client:
        response = client.get("/agencies/Abkürzung")

        assert response.json()["name"] == "Organisationsname"
        assert response.json()["abbreviation"] == "Abkürzung"
        assert response.json()["units"] == [
            {
                "name": "A I 1",
                "description": "Vom Fliegen und der Umwelt",
                "directorate_name": "A I",
                "directorate_description": "Unterabteilung Allgemeines",
                "directorate_general_name": "A",
                "directorate_general_description": "Abteilung Allgemeines",
                "tasks": [
                    {
                        "description": "Fliegen",
                        "subtasks": [
                            {"description": "Gute Nacht Geschichten"},
                            {"description": "Fluglotsentätigkeit"},
                        ],
                    },
                    {"description": "Nachhaltigkeit und Umwelt", "subtasks": None},
                ],
            },
            {
                "name": "A II 2",
                "description": "Noch ein Referat.",
                "directorate_general_name": None,
                "directorate_general_description": None,
                "directorate_name": None,
                "directorate_description": None,
                "tasks": [{"description": "Datenlabor aufbauen", "subtasks": None}],
            },
        ]


def test_non_existing_gvpl():
    with TestClient(app) as client:
        response = client.get("/agencies/missingagency")
        assert (
            response.status_code == 422
        )  # fastapi raises 422 if string that is not part of enum is used
