import os
import tempfile

import pytest

from kiras_api.settings import settings


settings.gvpl_files = ["./data/test-tasks.json"]

# this uses a local temporary folder to test model downloading and saving
# when developing locally

# if settings.environment == "local":
#    settings.data_dir = tempfile.TemporaryDirectory().name
