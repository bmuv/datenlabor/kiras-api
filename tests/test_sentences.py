from kiras_api.sentences import split_sentences_nltk


def test_sentence_splitter():
    text = (
        "Das ist ein Satz. Das ist ein zweiter Satz. Am 24. Dezember ist Weihnachten."
    )

    expected = [
        "Das ist ein Satz.",
        "Das ist ein zweiter Satz.",
        "Am 24. Dezember ist Weihnachten.",
    ]

    assert expected == split_sentences_nltk(text)

    # again to make sure that it also work when the model was not downloaded
    assert expected == split_sentences_nltk(text)
