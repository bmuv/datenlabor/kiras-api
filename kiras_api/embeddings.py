import os
import pickle
from functools import lru_cache
from pathlib import Path

import numpy as np
import pandas as pd
from sentence_transformers import SentenceTransformer, util

from kiras_api.models import (
    GVPL,
    Explanation,
    InvolvementResponse,
    InvolvementScore,
)
from kiras_api.sentences import split_sentences_nltk
from kiras_api.settings import settings
from kiras_api.tasks import split_subtasks


class EmbeddingAlgo:
    """Algorithm to determine relevant organizational units through semantic embeddings.

    Parameters
    ----------
    gvpls_: A list of GVPL.

    """

    def __init__(self, gvpls_):
        """Generate sentence-embedding algorithm."""
        # store model location
        self.embedding_model_location = (
            settings.data_dir + settings.model_dir + settings.embedding_model_name
        )

        # load model or download it
        self.model = self.load_model()

        # generate folder for GVPL embeddings.
        self.task_cache_folder = f"{settings.data_dir}kiras_api/{settings.environment}"
        if not os.path.exists(self.task_cache_folder):
            os.makedirs(self.task_cache_folder)

        # store the embedded GVPLs
        self.task_cache = {}
        # loop over all gvpls and embedded them if necessary
        for gvpl_ in gvpls_:
            abbreviation_ = gvpl_.abbreviation
            last_exported_ = gvpl_.last_exported
            # generate cache location
            cache_location_ = (
                self.task_cache_folder
                + "/"
                + f"tasks_{abbreviation_}_{last_exported_}_embedded.pkl"
            )
            if Path(cache_location_).is_file():
                with open(cache_location_, "rb") as f:
                    self.task_cache[abbreviation_] = pickle.load(f)
            else:
                self.task_cache[abbreviation_] = self.generate_task_cache(
                    gvpl_, cache_location_
                )

    def download_model(self):
        """Download model from web."""
        if (
            settings.proxy_username
            and settings.proxy_password
            and settings.proxy_location
            and settings.proxy_port
        ):
            proxy = f"http://{settings.proxy_username}:{settings.proxy_password}@{settings.proxy_location}:{settings.proxy_port}"

            os.environ["HTTP_PROXY"] = proxy
            os.environ["HTTPS_PROXY"] = proxy
        print("trying to download model")
        self.model = SentenceTransformer(settings.embedding_model_name)
        self.model.save(self.embedding_model_location)
        print(f"model saved to {self.embedding_model_location}")

    @lru_cache(maxsize=1)
    def load_model(self) -> SentenceTransformer:
        """Load model from disk (if available) or from internet."""
        try:
            return SentenceTransformer(self.embedding_model_location)
        except Exception:
            self.download_model()
            return SentenceTransformer(self.embedding_model_location)

    def clean_string(self, string: str) -> str:
        """Clean up common issues in doc texts."""
        replacement = {
            "\r": " ",
            "\n": " ",
            "II.": "",
            "III.": "",
            "sog.": "sogenannte",
            "co.": "co",
            "z. B.": "zum Beispiel",
            "ggf.": "gegebenenfalls",
            "ggfs.": "gegebenenfalls",
        }
        for key, value in replacement.items():
            string = string.replace(key, value)
        for number in range(1, 32):
            string_ = str(number) + ". "
            string = string.replace(string_, "")
        return string

    def generate_task_cache(self, gvpl: GVPL, cache_location: str):
        """Generate tasks embeddings & pickle dump.

        Returns
        -------
        task_cache =
        {
            unit_name: {
                'text': text of (sub-)task,
                'embedding': embedding of extended text
            }
        }

        """
        task_texts = split_subtasks(gvpl)

        task_cache = {}

        for unit in task_texts.keys():
            task_cache[unit] = {
                "text": task_texts[unit]["short"],
                "embedding": self.model.encode(task_texts[unit]["extended"]),
            }

        with open(cache_location, "wb") as file:
            pickle.dump(task_cache, file)

        return task_cache

    def __create_prediction_list(self, task_cache, document_embeddings):
        # construct full similarity matrix between all sentences and all Tasks
        # store document_index and unit_index as tuples to find sentences of specific
        # document and tasks of specific unit
        predictions = None
        # unit_index stores a tuple (start_index, stop_index) for each unit,
        # where "start_index" is the column index of the first task of the unit
        # and "stop_index" is the column index of the last taks of the unit.
        unit_counter = 0
        unit_index = []

        predictions = None
        # loop over units
        for unit in task_cache.keys():
            unit_index.append(
                (
                    unit_counter,
                    unit_counter + len(task_cache[unit]["embedding"]),
                )
            )
            unit_counter += len(task_cache[unit]["embedding"])
            # concatenate blocks of similarity along the columns for each unit
            if predictions is None:
                predictions = util.cos_sim(
                    document_embeddings, task_cache[unit]["embedding"]
                ).numpy()
            else:
                predictions = np.concatenate(
                    (
                        predictions,
                        util.cos_sim(
                            document_embeddings, task_cache[unit]["embedding"]
                        ).numpy(),
                    ),
                    axis=1,
                )

        return unit_index, predictions

    def __process_embeddings(
        self,
        task_cache,
        text_embeddings,
        normalize: bool = True,
        k: int = 10,
    ):
        units = list(task_cache.keys())
        unit_index, pred_array = self.__create_prediction_list(
            task_cache, text_embeddings
        )
        # normalize--or not
        normalized = pred_array
        if normalize:
            for i in range(normalized.shape[0]):
                normalized[i, :] = normalized[i, :] - np.mean(normalized[i, :])

        # loop over units
        pairs_df = pd.DataFrame(
            index=units, columns=["top_score", "sentence_i", "task_i"]
        )
        agg_scores = []

        for ref_index, ref_span in enumerate(unit_index):
            # find matrix of sentences in document and tasks in units
            block = normalized[:, ref_span[0] : ref_span[1]]

            # 1. find top sentence pairs in each unit-block
            best_pair_index = np.unravel_index(block.argmax(), block.shape)
            # index of (sentence, task)
            # if there is more than one pair with the same value,
            # choose the first one
            unit = units[ref_index]
            pairs_df.loc[unit, ["top_score", "sentence_i", "task_i"]] = [
                normalized[best_pair_index],
                best_pair_index[0],
                best_pair_index[1],
            ]
            # 2. find aggregate score
            agg_scores.append(np.sum(np.sort(block.flatten())[::-1][:k]))

        # convert aggregate scores into named Series
        scores_s = pd.Series(np.asarray(agg_scores), index=units)

        return pairs_df, scores_s

    def find_involvement(
        self,
        text: str,
        agency: str,
        return_n: int = 10,
    ) -> InvolvementResponse:
        """Match input text with tasks and return List of Units."""
        # load tasks from the correct cache
        if agency not in self.task_cache:
            raise Exception("agency not found")
        task_cache = self.task_cache[agency]
        # clean and split document text
        input_sentences = split_sentences_nltk(self.clean_string(text))
        # embed document text
        paragraph_embeddings = self.model.encode(input_sentences)

        pairs_df, scores_s = self.__process_embeddings(
            task_cache=task_cache, text_embeddings=paragraph_embeddings
        )
        # dataframe to collect all scoring info per ref
        refs_df = pairs_df.copy()
        refs_df["agg_score"] = scores_s
        refs_df["unit"] = refs_df.index
        refs_df["sentence"] = refs_df["sentence_i"].apply(lambda i: input_sentences[i])
        refs_df["task"] = refs_df.apply(
            lambda row: task_cache[row["unit"]]["text"][row["task_i"]],
            axis=1,
        )
        # process everything into InvolvementScores
        refs_df["explanation"] = refs_df.apply(
            lambda row: Explanation(
                task=row["task"],
                sentence=row["sentence"],
                score=row["top_score"],
            ),
            axis=1,
        )
        refs_df["score"] = refs_df.apply(
            lambda row: InvolvementScore(
                unit_name=row["unit"],
                score=row["agg_score"],
                explanations=[row["explanation"]],
            ),
            axis=1,
        )

        refs_df = refs_df.sort_values(by="agg_score", ascending=False).head(return_n)

        response = InvolvementResponse(text=text, scores=refs_df["score"].tolist())

        return response
