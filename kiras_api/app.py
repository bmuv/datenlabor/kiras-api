import logging
from contextlib import asynccontextmanager
from typing import Callable

from fastapi import APIRouter, FastAPI, HTTPException, Request, Response
from fastapi.middleware.cors import CORSMiddleware
from fastapi.routing import APIRoute
from starlette.background import BackgroundTask

from kiras_api.embeddings import EmbeddingAlgo
from kiras_api.models import (
    GVPL,
    Agencies,
    AgenciesResponse,
    AgenciesTemplate,
    Agency,
    Algorithms,
    InvolvementResponse,
)
from kiras_api.settings import settings
from kiras_api.tasks import read_tasks

gvpls = []


def log_info(req_body, res_body):
    """Write request and response body to log file."""
    logging.info("Request: %s", req_body)
    logging.info("Response: %s", res_body.decode("utf-8"))


class LoggingRoute(APIRoute):
    """Custom API Route to allow for request-response logging.

    Code from here: https://stackoverflow.com/questions/69670125/how-to-log-raw-http-request-response-in-python-fastapi/73464007#73464007
    """

    def get_route_handler(self) -> Callable:
        """Add logging to API."""
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            req_path = request.url
            response = await original_route_handler(request)

            res_body = response.body
            response.background = BackgroundTask(log_info, req_path, res_body)

            return response

        return custom_route_handler


@asynccontextmanager
async def lifespan(app: FastAPI):
    """Read GVPLs and embedded them at startup."""
    # check available GVPLs
    for path in settings.gvpl_files:
        tasks = read_tasks(path)
        if not isinstance(tasks, GVPL):
            continue
        if tasks not in gvpls:
            gvpls.append(tasks)
    # generate available Agencies
    # slightly complex code explained here: https://gist.github.com/myuanz/03f3e350fb165ec3697a22b559a7eb50
    new_enum = AgenciesTemplate(
        "Agencies", {gvpl.name: gvpl.abbreviation for gvpl in gvpls}
    )
    Agencies._member_map_ = new_enum._member_map_
    Agencies._member_names_ = new_enum._member_names_
    Agencies._value2member_map_ = new_enum._value2member_map_
    # generate embedding algorithm
    app.embedding_algo = EmbeddingAlgo(gvpls)
    yield


app = FastAPI(lifespan=lifespan)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:4200"],
    allow_origin_regex=settings.cors_regex,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
logging.basicConfig(
    filename=settings.data_dir + settings.logfile_name,
    level=logging.INFO,
    encoding="utf-8",
    format="%(asctime)s %(levelname)s %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)

router = APIRouter(route_class=LoggingRoute)


@router.get("/involvement")
async def involvement(
    text: str,
    return_n: int = 10,
    method: Algorithms = Algorithms.embeddings.value,
    agency: Agencies = settings.default_agency,
) -> InvolvementResponse:
    """Return score relative to input text."""
    if not text.strip():
        raise HTTPException(status_code=400, detail="Please provide a text.")
    try:
        return app.embedding_algo.find_involvement(
            text=text, agency=agency, return_n=return_n
        )
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))


@router.get("/agencies")
async def agencies() -> AgenciesResponse:
    """Return list of available Agencies."""
    response = []
    for gvpl in gvpls:
        response.append(
            Agency(
                name=gvpl.name,
                abbreviation=gvpl.abbreviation,
                last_exported=gvpl.last_exported,
            )
        )
    return AgenciesResponse(agencies=response, default=settings.default_agency)


@router.get("/agencies/{abbreviation}")
def units(abbreviation: Agencies) -> GVPL:
    """Return gvpl of one agency."""
    gvpl = [gvpl for gvpl in gvpls if gvpl.abbreviation == abbreviation]
    if len(gvpl) != 1:
        raise Exception("Found more than one gvpl with abbreviation.")
    return gvpl[0]


app.include_router(router)
