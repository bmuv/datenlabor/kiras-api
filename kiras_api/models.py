from enum import Enum
from typing import List, Optional

from pydantic import BaseModel


class Subtask(BaseModel):
    """Subtask."""

    description: str


class Task(BaseModel):
    """Task."""

    description: str
    subtasks: Optional[List[Subtask]] = None


class Unit(BaseModel):
    """Unit."""

    name: str
    description: Optional[str]
    directorate_general_name: str | None = None
    directorate_general_description: str | None = None
    directorate_name: str | None = None
    directorate_description: str | None = None

    tasks: Optional[List[Task]]


class GVPL(BaseModel):
    """Export model for kiras_api."""

    units: List[Unit]
    name: str
    abbreviation: str
    last_exported: str


class Algorithms(str, Enum):
    """Set of algorithms.

    See here for the Enum logic:
    https://stackoverflow.com/questions/58608361/string-based-enum-in-python.
    """

    embeddings = "embeddings"


class AgenciesTemplate(str, Enum):
    """Container to store all available Agencies for the API.

    See here: https://github.com/tiangolo/fastapi/issues/13#issuecomment-918323671.
    """

    pass


class Agencies(str, Enum):
    """Container will be filled on startup with all available Agencies.

    See here: https://myuan.fun/dynamic-enum-with-fastapi/.
    """

    pass


class Explanation(BaseModel):
    """Reasons for the match."""

    task: str
    sentence: str
    score: float


class InvolvementScore(BaseModel):
    """Unit score for a given text."""

    unit_name: str
    score: float
    explanations: Optional[List[Explanation]] = None


class InvolvementResponse(BaseModel):
    """Response from involvement api."""

    text: str
    scores: List[InvolvementScore]


class Agency(BaseModel):
    """Container for specific Agency."""

    name: str
    abbreviation: str
    last_exported: str


class AgenciesResponse(BaseModel):
    """Response from agencies api."""

    agencies: List[Agency]
    default: str
