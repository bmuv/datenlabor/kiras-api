"""loads settings and credentials."""

from typing import Optional

from pydantic import BaseSettings


class Settings(BaseSettings):
    """sets settings from env vars or file."""

    environment: str = "testing"

    gvpl_files: list = [
        "/data/test-tasks.json"
    ]
    default_agency: str = "BMUV"

    data_dir: str = "/data/"
    model_dir: str = "models/"

    logfile_name: str = "info.log"

    embedding_model_name: str = "distiluse-base-multilingual-cased-v1"
    sentence_model_name: str = "punkt"

    # if a proxy is used, these must be set in env_file or as environment variables
    # leave blank if no proxy is used
    proxy_username: Optional[str] = None
    proxy_password: Optional[str] = None
    proxy_location: Optional[str] = None
    proxy_port: Optional[str] = None

    cors_regex: str = "https://.*.office.dir"

    class Config:
        """defines file name."""

        # requires a local .env adapted from .env-example
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()
