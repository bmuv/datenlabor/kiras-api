"""Read tasks from file."""
import json
import logging
import sys
import unicodedata
from typing import List, Mapping

from stopwordsiso import stopwords

from kiras_api.models import GVPL

# define translation dictionary to remove punctuation
tbl = dict.fromkeys(
    i for i in range(sys.maxunicode) if unicodedata.category(chr(i)).startswith("P")
)


def clean_string(paragraph: str, stopwords: List[str] = stopwords("de")) -> list:
    """Split string into words, remove punctuation and stop-words.

    Argument:
    ---------
        paragraph (str): Piece of text to be cleaned and separated.


    Returns
    -------
        list of significant words in paragraph.

    """
    output = paragraph.replace("/", " ").split()

    output = [
        entry.lower().translate(tbl)
        for entry in output
        if entry.lower().translate(tbl) not in stopwords
    ]

    return output


def read_tasks(path: str) -> GVPL | None:
    """Read tasks from file."""
    try:
        with open(path, "r", encoding="utf-8") as f:
            return GVPL(**json.load(f))
    except Exception as e:
        print(
            f"The file at '{path}' could not be successfully parsed "
            "into the present GVPL format: " + str(e)
        )
        logging.error(
            f"The file at '{path}' could not be successfully parsed "
            "into the present GVPL format: " + str(e)
        )
        return None


def join_subtasks(gvpl: GVPL) -> Mapping[str, List[List[str]]]:
    """Join subtasks. See also unit tests for output format."""
    task_simplified = {}
    for unit in gvpl.units:
        tasks_list = []
        for task in unit.tasks:
            description = unit.description + " " + task.description
            if task.subtasks is not None:
                for subtask in task.subtasks:
                    description += " " + subtask.description
            tasks_list.append(description)
        task_simplified[unit.name] = [clean_string(entry) for entry in tasks_list]
    return task_simplified


def split_subtasks(gvpl: GVPL) -> Mapping[str, List[str]]:
    """Split subtasks into separate entries.

    Returns
    -------
    tasks_text =
    {
        unit_name: {
            'short': text of task,
            'extended': text of unit + task
        }
    }

    """
    tasks_text = {}

    for unit in gvpl.units:
        name = unit.name
        texts = []
        texts_long = []
        if unit.description is not None:
            unit_description = unit.description
        else:
            unit_description = ""
        if unit.tasks is not None:
            for task in [a for a in unit.tasks if "des Referats" not in a.description]:
                if task.description is not None:
                    description = task.description
                else:
                    description = ""
                if task.subtasks is not None:
                    for subtask in [
                        a for a in task.subtasks if "des Referats" not in a.description
                    ]:
                        texts.append(description + ". " + subtask.description)
                        texts_long.append(
                            unit_description
                            + ". "
                            + description
                            + ". "
                            + subtask.description
                        )
                else:
                    texts.append(description)
                    texts_long.append(unit_description + ". " + description)
            tasks_text[name] = {"short": texts, "extended": texts_long}
    return tasks_text
