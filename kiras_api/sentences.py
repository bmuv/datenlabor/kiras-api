import os
from typing import List

import nltk
from nltk import tokenize

from kiras_api.settings import settings


def construct_model_location():
    """Return sentence model location."""
    loc = os.path.abspath(
        os.path.join(
            settings.data_dir, settings.model_dir, settings.sentence_model_name
        )
    )
    return loc


def download_model():
    """Download nltk model."""
    if (
        settings.proxy_username
        and settings.proxy_password
        and settings.proxy_location
        and settings.proxy_port
    ):
        proxy = f"http://{settings.proxy_username}:{settings.proxy_password}@{settings.proxy_location}:{settings.proxy_port}"
        nltk.set_proxy(proxy)

    download_successful = nltk.download(
        info_or_id=settings.sentence_model_name,
        download_dir=construct_model_location(),
    )
    if download_successful:
        print("download successful.")
    else:
        raise ConnectionError("Could not download nltk model.")


def load_model():
    """Load model into memory."""
    try:
        nltk.data.find(
            f"tokenizers/{settings.sentence_model_name}",
            paths=[construct_model_location()],
        )
    except LookupError as e:
        print(e)
        download_model()
        nltk.data.find(
            f"tokenizers/{settings.sentence_model_name}",
            paths=[construct_model_location()],
        )
    nltk.data.path.append(construct_model_location())


def split_sentences_nltk(text: str) -> List[str]:
    """Split text into sentences using nltk."""
    return tokenize.sent_tokenize(text, language="german")


load_model()
