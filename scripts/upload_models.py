"""Model Uploader."""

import os
from pathlib import Path

import typer

from kiras_api import embeddings, sentences


def main(
    ssh_key_location: Path = Path.home() / ".ssh" / "rsa-key",
    host: str = "test-datenlabor.office.dir",
):
    """Run.

    Download and transfer models. Leave local download on disk.
    """
    embeddings.load_model()
    embeddings_location = embeddings.embedding_model_location()

    sentences.load_model()
    sentence_model_location = sentences.sentences_model_location()
    print("attempt upload via ssh")
    scp_command = f"scp -i {ssh_key_location} -P 2222 -r {embeddings_location} {sentence_model_location} user@{host}:/data-dev/models"  # noqa: E501
    os.system(scp_command)


if __name__ == "__main__":
    typer.run(main)()
